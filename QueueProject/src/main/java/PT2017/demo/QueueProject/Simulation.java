package PT2017.demo.QueueProject;
import javax.swing.SwingUtilities;

import gui.*;

public class Simulation {

	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Gui();
			}
		});
		
	}

}
