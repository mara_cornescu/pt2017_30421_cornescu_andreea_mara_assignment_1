package gui;

import queue.simulation.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.*;

/**
 * 
 * The following class is used in order to provide the graphical user interface. It contains as private instance 
 * variables five panels (panelInput, panelButtons, panelTextArea, panelStatistics, panelFin). There is a text field 
 * for all the information that the user needs to insert and buttons for starting the simulation, stopping if needed, 
 * resetting all the queues, displaying the statistics and the entire evolution of the queues.  A constructor used to 
 * initialize and prepare the new object for use. Action listeners have been added to the operation buttons in order 
 * to implement their functionality and all the components were included to the final panel.
 *
 */

public class Gui extends JFrame {
	
	private boolean stop;
	private JPanel panelInput, panelButtons, panelTextArea, panelStatistics, panelFin;
	private JLabel labelNumberOfQueues, labelMinArrivalTime, labelMaxArrivalTime, labelMinServiceTime, labelMaxServiceTime, labelSimulationTime;
	private JLabel labelAvgWaitingTime, labelAvgServiceTime, labelEmpty, labelPeakNumber, labelPeakHour;
	private JTextField textNumberOfQueues, textMinArrivalTime, textMaxArrivalTime, textMinServiceTime, textMaxServiceTime, textSimulationTime;
	private JTextField textAvgWaitingTime, textAvgServiceTime, textEmpty, textPeakNumber, textPeakHour;
	private JButton startSimulation, stopSimulation, resetSimulation, display, queueEvolution;
	private JTextArea[] area;
	private Logic simulation;
	
	public Gui() {
		
		super("Queue simulation");
		setLayout(new GridLayout());
		setSize(800,480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
		panelFin = new JPanel();
		panelInput = new JPanel(new GridLayout(3,2));
		panelButtons = new JPanel();
		panelTextArea = new JPanel();
		panelTextArea.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		panelTextArea.setPreferredSize(new Dimension(750, 250));
		panelStatistics = new JPanel();
		panelStatistics.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		panelStatistics.setPreferredSize(new Dimension(750, 80));
		
		labelNumberOfQueues = new JLabel("Number of queues: ");
		labelMinArrivalTime = new JLabel("Minimum arrival time: ");
		labelMaxArrivalTime = new JLabel("Maximum arrival time: ");
		labelMinServiceTime = new JLabel("Minimum service time: ");
		labelMaxServiceTime = new JLabel("Maximum service time: ");
		labelSimulationTime = new JLabel("SimulationTime: ");
		
		labelAvgWaitingTime = new JLabel("Average waiting time: ");
		labelAvgServiceTime = new JLabel("Average service time: ");
		labelEmpty = new JLabel("Empty queue time: ");
		labelPeakHour = new JLabel("Peak moment: ");
		labelPeakNumber = new JLabel("Peak number of clients: ");
		
		textNumberOfQueues = new JTextField(5);
		textMinArrivalTime = new JTextField(5);
		textMaxArrivalTime = new JTextField(5);
		textMinServiceTime = new JTextField(5);
		textMaxServiceTime = new JTextField(5);
		textSimulationTime = new JTextField(5);
		
		textAvgWaitingTime = new JTextField(5);
		textAvgServiceTime = new JTextField(5);
		textEmpty = new JTextField(5);
		textPeakHour = new JTextField(5);
		textPeakNumber = new JTextField(5);
		
		textAvgWaitingTime.setEditable(false);
		textAvgServiceTime.setEditable(false);
		textEmpty.setEditable(false);
		textPeakHour.setEditable(false);
		textPeakNumber.setEditable(false);
		
		startSimulation = new JButton("Start");
		stopSimulation = new JButton("Stop");
		resetSimulation = new JButton("Reset");
		display = new JButton("Display Statistics");
		queueEvolution = new JButton("Queue Evolution");
		
		panelInput.add(labelNumberOfQueues);
		panelInput.add(textNumberOfQueues);
		panelInput.add(labelMinArrivalTime);
		panelInput.add(textMinArrivalTime);
		panelInput.add(labelMaxArrivalTime);
		panelInput.add(textMaxArrivalTime);
		panelInput.add(labelMinServiceTime);
		panelInput.add(textMinServiceTime);
		panelInput.add(labelMaxServiceTime);
		panelInput.add(textMaxServiceTime);
		panelInput.add(labelSimulationTime);
		panelInput.add(textSimulationTime);
		
		panelStatistics.add(labelAvgWaitingTime);
		panelStatistics.add(textAvgWaitingTime);
		panelStatistics.add(labelAvgServiceTime);
		panelStatistics.add(textAvgServiceTime);
		panelStatistics.add(labelEmpty);
		panelStatistics.add(textEmpty);
		panelStatistics.add(labelPeakHour);
		panelStatistics.add(textPeakHour);
		panelStatistics.add(labelPeakNumber);
		panelStatistics.add(textPeakNumber);
		
				
		startSimulation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				stop = false;
				
				try {
					final int numberOfQueues = Integer.parseInt(textNumberOfQueues.getText());
					int minArrivalTime = Integer.parseInt(textMinArrivalTime.getText());
					int maxArrivalTime = Integer.parseInt(textMaxArrivalTime.getText());
					int minServiceTime = Integer.parseInt(textMinServiceTime.getText());
					int maxServiceTime = Integer.parseInt(textMaxServiceTime.getText());
					int simulationTime = Integer.parseInt(textSimulationTime.getText());
					
					simulation = new Logic(numberOfQueues, minArrivalTime, maxArrivalTime, minServiceTime, maxServiceTime, simulationTime);
					simulation.startSimulation();
					
					Thread t = new Thread(new Runnable() {
						public void run() {
									
							area = new JTextArea[numberOfQueues];
							for(int i = 0; i<numberOfQueues; i++) {
								area[i] = new JTextArea();
								area[i].setBorder(BorderFactory.createLineBorder(Color.black));
								panelTextArea.add(area[i]);
							}
							
							int j = 0;
							
							while(j < simulation.getWatchSize()) {
								
								if(stop) break;
								
								for(int i = 0; i<numberOfQueues; i++)	{	
									area[i].setText(simulation.getWatch(j));
									j++;
								}
								
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
								}
							}
						}
					});
					
					t.start();
				
					
				}
				catch(NumberFormatException err) {
					JOptionPane.showMessageDialog(null, "Incorrect input data!");
				}
				
			}
		});
		
		
		stopSimulation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stop = true;
			}
		});
		
		resetSimulation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelTextArea.removeAll();
				panelTextArea.repaint();
				panelTextArea.revalidate();
				
				textAvgWaitingTime.setText("");
				textAvgServiceTime.setText("");
				textEmpty.setText("");
				textPeakHour.setText("");
				textPeakNumber.setText("");
			}
		});
		
		display.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					textAvgWaitingTime.setText(simulation.getAverageWaitingTime());
					textAvgServiceTime .setText(simulation.getAverageServiceTime());
					textEmpty.setText("" + simulation.getEmptyQueueTime());
					textPeakHour.setText("" + simulation.getPeakHour());
					textPeakNumber.setText("" + simulation.getPeakNumberOfClients());
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
		
		
		queueEvolution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					JFrame outputFrame = new JFrame("Queue Evolution");
					JTextArea finalOutput = new JTextArea();
					JScrollPane scrollPane = new JScrollPane(finalOutput);
					scrollPane.setBounds(5, 5, 300, 300);
					outputFrame.add(scrollPane);
		
					for(int i=0; i<simulation.getWatchSize(); i++)
						finalOutput.append(simulation.getWatch(i) + "\n");
				
					outputFrame.setSize(400, 400);
					outputFrame.setResizable(false);
					outputFrame.setVisible(true);
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
		
		panelButtons.add(startSimulation);
		panelButtons.add(resetSimulation);
		panelButtons.add(stopSimulation);
		
		panelStatistics.add(display);
		panelStatistics.add(queueEvolution);
		
		//panelFin.add(panelTextArea);
		//panelFin.add(area2);
		panelFin.add(panelInput);
		panelFin.add(panelButtons);
		panelFin.add(panelTextArea);
		panelFin.add(panelStatistics);
		
		
		add(panelFin);
		
		setVisible(true);
	}
}
