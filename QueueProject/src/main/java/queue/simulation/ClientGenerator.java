package queue.simulation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The ClientGenerator class is used to generate randomly a certain number of clients, depending on the
 * simulation time. All the clients are added to an arrayList, and will be extracted later, when the 
 * simulation begins. The class provides only the constructor, in which the clients are created, and one 
 * method to take all the clients.
 */

public class ClientGenerator {

	private List<Client> allClients;
	
	ClientGenerator(int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime, int simulationTime) {
		
		this.allClients = new ArrayList<Client>();
		
		Client client;
		int index = 1;
		int currentTime = 0;
		Random randomArrivalTime = new Random();
		Random randomServiceTime = new Random();
		
		while(currentTime < simulationTime) {
			
			int arrivalTime = randomArrivalTime.nextInt(maxArrivalTime - minArrivalTime + 1) + minArrivalTime;
			int serviceTime = randomServiceTime.nextInt(maxServiceTime - minServiceTime + 1) + minServiceTime;
			
			if(arrivalTime + currentTime> simulationTime)
				break;
			
			client = new Client(String.valueOf(index), arrivalTime + currentTime, serviceTime);
			index++;
			allClients.add(client);
			currentTime ++;
		}
	}
	
	public List<Client> getAllClients() {
		return allClients;
	}
	
}
