 package queue.simulation;

/**
 * The Client class implements a client, who has three fundamental
 * characteristics: id, arrival time and service time. Setters and 
 * getters are provided for each parameter. Also, the class includes
 * the method toString , which is used to give a string representation
 * of an object, when needed.
 */

public class Client {
	
	private String id;
	private int arrivalTime;
	private int serviceTime;
	
	public Client(String id, int arrivalTime, int serviceTime) {
		setId(id);
		setArrivalTime(arrivalTime);
		setServiceTime(serviceTime);
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
	
	public int getServiceTime() {
		return serviceTime;
	}
	
	public String toString() {
		return 
			String.format("Client %s: arrival time - %d, service time - %d", getId(), getArrivalTime(), getServiceTime());
	}
	
}
