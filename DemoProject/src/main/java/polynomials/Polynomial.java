package polynomials;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The Polynomial class constructs the polynomial as a list
 * of monomials and implements the basic operations that can
 * be performed on polynomials: addition, subtraction,
 * multiplication, division, integration and differentiation.
 * This class also provides a method to sort the list of monomials
 * decreasingly and the toString method.
 */

public class Polynomial {
	
	private List<Monomial> polinom;
	
	public Polynomial() {
		this.polinom = new ArrayList<Monomial>();
	}	
	
	public void sort() {
		Collections.sort(polinom, new PolComparator());
	}
	
	
	/*
	 * The following method performs the addition of one monomial
	 * to the list of monomials, resulting one polynomial.
	 */
	
	public void addMonomialToPolinom(Monomial mon) {
		for(Monomial m: polinom)
			if(m.getDegree() == mon.getDegree()) {
				m.addMonomial(mon);
				return;
			}
		polinom.add(mon);
	}
	
	public void addition(Polynomial p) {
		for(Monomial m: p.polinom) {
			addMonomialToPolinom(m);
		}
	}
	
	public void subtraction(Polynomial p) {
		for(Monomial m: p.polinom) {
			m.setCoefficient(0-m.getCoefficient().intValue());
			addMonomialToPolinom(m);
		}
	}
	
	public void multiply(Polynomial p) {
		Polynomial result = new Polynomial();
		for(Monomial m1: polinom)
			for(Monomial m2: p.polinom)
				result.addMonomialToPolinom(new IntMonomial(m1.getDegree()+m2.getDegree(),
														m1.getCoefficient().intValue()*m2.getCoefficient().intValue()));
		polinom = result.polinom;
	} 
	
	/*
	 * The division operation is based on the polynomial long division algorithm, which takes a polynomial 
	 * called the dividend, and another polynomial called the divisor.The result will be composed of the 
	 * quotient and the remainder and will be displayed as a String. If there is no remainder, "0" will be 
	 * displayed.
	 */
	
	public String division(Polynomial p) {
		
		Polynomial quotient = new Polynomial();
		Polynomial remainder = new Polynomial();
	
		Monomial firstTerm = new DoubleMonomial
				(p.polinom.get(0).getDegree(), p.polinom.get(0).getCoefficient().doubleValue());
		
		for (Monomial m2: polinom) {
			Polynomial impartire = new Polynomial();
			
			int degree =  m2.getDegree()-firstTerm.getDegree();
			double coefficient = m2.getCoefficient().doubleValue()/firstTerm.getCoefficient().doubleValue();
			
			if(degree < 0) break;
			
			if(coefficient - (int)coefficient != 0) {
				Monomial monom = new DoubleMonomial(degree, coefficient);
				impartire.addMonomialToPolinom(monom);
			}
			else {
				Monomial monom = new IntMonomial(degree, (int)coefficient);
				impartire.addMonomialToPolinom(monom);
			}
	
			quotient.addition(impartire);
			impartire.multiply(p);
			subtraction(impartire);
			remainder = this;
			//if (remainder.polinom.get(0).getDegree() < firstTerm.getDegree()) break;
		}
		
		if(remainder.toString().equals("")) 
			return ("quotient = " + quotient.toString() + " " + "\nremainder = 0");
		return ("quotient = " + quotient + " " + "\nremainder = "+ remainder);
	}
	
	
	public void differentiation() {
		Polynomial result = new Polynomial();
		for(Monomial m: polinom)
			if(m.getDegree()==0) break;
			else 
				result.addMonomialToPolinom(new IntMonomial(m.getDegree()-1, m.getCoefficient().intValue()*m.getDegree()));
		polinom = result.polinom;
	}
		
	
	/*
	 * The following method performs the integration of a polynomial and it checks
	 * if the resulting coefficient is integer or double. The result can have both
	 * integer and real coefficients.
	 */
	public void integration() {
		
		Polynomial result = new Polynomial();
		
		for(Monomial m: polinom) {
			
			int degree =  m.getDegree()+1;
			double coefficient = m.getCoefficient().doubleValue()/(m.getDegree()+1);
				
			if(coefficient - (int)coefficient != 0) {
				Monomial monom = new DoubleMonomial(degree, coefficient);
				result.addMonomialToPolinom(monom);
			}
			else {
				Monomial monom = new IntMonomial(degree, (int)coefficient);
				result.addMonomialToPolinom(monom);
			}
		}
		polinom  = result.polinom;
	}

	
	public String toString() {
		String result = "";
		boolean pass = false; //verificam daca e plus primu caracter
		for(Monomial p: polinom) {
			if(pass == false){
				result += p;
				pass = true;
			}
			else
				if(p.getCoefficient().doubleValue() > 0)
					result += "+" + p;
				else
					result += p;
			}
		return result;
	}
	
}
