package polynomials;

/**
 * The DoubleMonomial class is a subclass of the superclass Monomial
 * which has a double coefficient. It provides a constructor, implements 
 * the addition of two real monomials and the method toString, which
 * is used when a string representation of an object is needed.
 */

public class DoubleMonomial extends Monomial {
	
	public DoubleMonomial(int degree, Double coefficient) {
		setDegree(degree);
		setCoefficient(coefficient);
	}
	
	public void addMonomial(Monomial m) { 
			setCoefficient(getCoefficient().doubleValue() + m.getCoefficient().doubleValue());
	}
	
	public String toString() {
		
		if(getCoefficient().doubleValue()==0) 
			return "";
		
		if(getCoefficient().doubleValue()==-1 && getDegree()==0) 
			return String.format("-1");
				else if(getCoefficient().doubleValue()==-1 && getDegree()==1) 
					return String.format("-x");
					else if(getCoefficient().doubleValue()==-1) 
						return String.format("-x^%d", getDegree());
		
		if(getCoefficient().doubleValue()==1 && getDegree()==0) 
			return String.format("1");
				else if(getCoefficient().doubleValue()==1 && getDegree()==1) 
					return String.format("x");
						else if(getCoefficient().doubleValue()==1) 
							return String.format("x^%d", getDegree());
		
		if(getDegree()==0) 
			return String.format("%1.2f", getCoefficient());
		
		if(getDegree()==1) 
			return String.format("%1.2fx", getCoefficient());
		
		return String.format("%1.2fx^%d", getCoefficient(), getDegree());
}
	
}
