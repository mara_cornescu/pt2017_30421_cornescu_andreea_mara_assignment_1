package polynomials;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * JUnit test is used in order to verify the correctness of each method
 * and shows if the result is the expected one.
 *
 */

public class PolTest {

	@Test
	public void monomialAdditiontest() {
		
		Monomial m1 = new IntMonomial(2,-4);
		Monomial m2 = new IntMonomial(2,3);
		m1.addMonomial(m2);
		
		assertEquals("-x^2","" + m1);
		
	}
	
	@Test
	public void addMonomialToPolynomialtest() {
		
		Monomial m1 = new IntMonomial(2,-4);
		Monomial m2 = new IntMonomial(2,3);
		Monomial m3 = new DoubleMonomial(1,1.5);
		Monomial m4 = new IntMonomial(0,7);
		
		Polynomial p = new Polynomial();
		p.addMonomialToPolinom(m1);
		p.addMonomialToPolinom(m2);
		p.addMonomialToPolinom(m3);
		p.addMonomialToPolinom(m4);
		
		assertEquals("-x^2+1.50x+7","" + p);
		
	}
	
	@Test
	public void additionPolynomialtest() {
		
		Monomial m1 = new IntMonomial(2,1);
		Monomial m2 = new IntMonomial(1,-2);
		Monomial m3 = new IntMonomial(0,4);
		Monomial m4 = new IntMonomial(7,1);
		Monomial m5 = new IntMonomial(3,6);
		Monomial m6 = new IntMonomial(2,-2);
		Monomial m7 = new IntMonomial(0,1);
		
		Polynomial p1 = new Polynomial();
		p1.addMonomialToPolinom(m1);
		p1.addMonomialToPolinom(m2);
		p1.addMonomialToPolinom(m3);
		
		Polynomial p2 = new Polynomial();
		p2.addMonomialToPolinom(m4);
		p2.addMonomialToPolinom(m5);
		p2.addMonomialToPolinom(m6);
		p2.addMonomialToPolinom(m7);
		
		p1.addition(p2);
		p1.sort();
		
		assertEquals("x^7+6x^3-x^2-2x+5","" + p1);
		
	}
	
	@Test
	public void subtractionPolynomialtest() {
		
		Monomial m1 = new IntMonomial(2,1);
		Monomial m2 = new IntMonomial(1,-2);
		Monomial m3 = new IntMonomial(0,4);
		Monomial m4 = new IntMonomial(7,1);
		Monomial m5 = new IntMonomial(3,6);
		Monomial m6 = new IntMonomial(2,-2);
		Monomial m7 = new IntMonomial(0,1);
		
		Polynomial p1 = new Polynomial();
		p1.addMonomialToPolinom(m1);
		p1.addMonomialToPolinom(m2);
		p1.addMonomialToPolinom(m3);
		
		Polynomial p2 = new Polynomial();
		p2.addMonomialToPolinom(m4);
		p2.addMonomialToPolinom(m5);
		p2.addMonomialToPolinom(m6);
		p2.addMonomialToPolinom(m7);
		
		p1.subtraction(p2);
		p1.sort();
		
		assertEquals("-x^7-6x^3+3x^2-2x+3","" + p1);
		
	}
	
	@Test
	public void multiplyPolynomialtest() {
		
		Monomial m1 = new IntMonomial(2,1);
		Monomial m2 = new IntMonomial(1,-2);
		Monomial m3 = new IntMonomial(0,4);
		Monomial m4 = new IntMonomial(3,1);
		Monomial m5 = new IntMonomial(2,6);
		Monomial m6 = new IntMonomial(1,-2);
		Monomial m7 = new IntMonomial(0,1);
		
		Polynomial p1 = new Polynomial();
		p1.addMonomialToPolinom(m1);
		p1.addMonomialToPolinom(m2);
		p1.addMonomialToPolinom(m3);
		
		Polynomial p2 = new Polynomial();
		p2.addMonomialToPolinom(m4);
		p2.addMonomialToPolinom(m5);
		p2.addMonomialToPolinom(m6);
		p2.addMonomialToPolinom(m7);
		
		p1.multiply(p2);;
		p1.sort();
		
		assertEquals("x^5+4x^4-10x^3+29x^2-10x+4","" + p1);
		
	}
	
	@Test
	public void divisionPolynomialtest() {
		
		Monomial m1 = new IntMonomial(4,1);
		Monomial m2 = new IntMonomial(3,5);
		Monomial m3 = new IntMonomial(2,6);
		Monomial m4 = new IntMonomial(1,-4);
		Monomial m5 = new IntMonomial(0,11);
		Monomial m6 = new IntMonomial(2,1);
		Monomial m7 = new IntMonomial(1,-3);
		Monomial m8 = new IntMonomial(0,1);
		
		Polynomial p1 = new Polynomial();
		p1.addMonomialToPolinom(m1);
		p1.addMonomialToPolinom(m2);
		p1.addMonomialToPolinom(m3);
		p1.addMonomialToPolinom(m4);
		p1.addMonomialToPolinom(m5);
		
		Polynomial p2 = new Polynomial();
		p2.addMonomialToPolinom(m6);
		p2.addMonomialToPolinom(m7);
		p2.addMonomialToPolinom(m8);
		
		String s;
		s = p1.division(p2);
		
		assertEquals("quotient = x^2+8x+29 \nremainder = +75x-18","" + s);
		
	}
	
	@Test
	public void differentiationPolynomialtest() {
		
		Monomial m1 = new IntMonomial(7,3);
		Monomial m2 = new IntMonomial(2,2);
		Monomial m3 = new IntMonomial(1,-4);
		Monomial m4 = new IntMonomial(0,9);
		Monomial m5 = new IntMonomial(5,1);
		
		Polynomial p1 = new Polynomial();
		p1.addMonomialToPolinom(m1);
		p1.addMonomialToPolinom(m2);
		p1.addMonomialToPolinom(m3);
		p1.addMonomialToPolinom(m4);
		p1.addMonomialToPolinom(m5);
		p1.sort();
		
		p1.differentiation();
		
		
		assertEquals("21x^6+5x^4+4x-4","" + p1);
		
	}
	
	@Test
	public void integrationPolynomialtest() {
		
		Monomial m1 = new IntMonomial(7,3);
		Monomial m2 = new IntMonomial(2,6);
		Monomial m3 = new IntMonomial(1,-4);
		Monomial m4 = new IntMonomial(0,9);
		Monomial m5 = new IntMonomial(5,1);
		
		Polynomial p1 = new Polynomial();
		p1.addMonomialToPolinom(m1);
		p1.addMonomialToPolinom(m2);
		p1.addMonomialToPolinom(m3);
		p1.addMonomialToPolinom(m4);
		p1.addMonomialToPolinom(m5);
		p1.sort();
		
		p1.integration();
		
		
		assertEquals("0.38x^8+0.17x^6+2x^3-2x^2+9x","" + p1);
		
	}
	
}
