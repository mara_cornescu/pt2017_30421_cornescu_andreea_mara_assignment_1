package polynomials;

/**
 * The IntMonomial class is a subclass of the superclass Monomial
 * which has a integer coefficient. It provides a constructor, 
 * implements the addition of two monomials and the method toString, 
 * which is used when a string representation of an object is needed.
 */

public class IntMonomial extends Monomial {
	
	public IntMonomial(int degree, Integer coefficient) {
		setDegree(degree);
		setCoefficient(coefficient);
	}
	
	public void addMonomial(Monomial m) {
			setCoefficient(getCoefficient().intValue() + m.getCoefficient().intValue());
	}
	
	public String toString() {
		
		if(getCoefficient().intValue()==0) 
			return "";
		
		if(getCoefficient().intValue()==-1 && getDegree()==0) 
			return String.format("-1");
				else if(getCoefficient().intValue()==-1 && getDegree()==1) 
					return String.format("-x");
						else if(getCoefficient().intValue()==-1) 
							return String.format("-x^%d", getDegree());
		
		if(getCoefficient().intValue()==1 && getDegree()==0) 
			return String.format("1");
				else if(getCoefficient().intValue()==1 && getDegree()==1) 
					return String.format("x");
						else if(getCoefficient().intValue()==1) 
							return String.format("x^%d", getDegree());
		
		if(getDegree()==0) 
			return String.format("%d", getCoefficient());
		
		if(getDegree()==1) 
			return String.format("%dx", getCoefficient());
		
		return String.format("%dx^%d", getCoefficient(), getDegree());
		
	}
	
}
