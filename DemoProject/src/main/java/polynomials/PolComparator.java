package polynomials;
import java.util.Comparator;

/**
 * This class is used to compare two monomials in order to sort
 * them decreasingly. It implements the Comparator interface.
 */

public class PolComparator implements Comparator<Monomial> {

	public int compare(Monomial monOne, Monomial monTwo) {
		
		if(monOne.getDegree() < monTwo.getDegree())
			return 1;
		else
			return -1;
		
	}

}