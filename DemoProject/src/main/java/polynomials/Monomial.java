package polynomials;

/** 
 * The Monomial abstract class implements a single term of a polynomial.
 * It has two private instance variables (degree and coefficient) and
 * provides getting and setting methods for each variable and abstract
 * methods that will be implemented in its subclasses.
 */

public abstract class Monomial {
	
	private int degree;
	private Number coefficient;
	
	public Number getCoefficient() {
		return coefficient;
	} 
	
	public void setCoefficient(Number coefficient) {
		this.coefficient = coefficient;
	}
	
	public int getDegree() {
		return degree;
	}
	
	public void setDegree(int degree) {
		if (degree < 0)
			throw new IllegalArgumentException 
					("Degree of a polynomial cannot be negative!");
		this.degree = degree;
	}
	
	public abstract void addMonomial(Monomial m);
	public abstract String toString();
	
}
