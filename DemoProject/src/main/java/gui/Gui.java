package gui;

import polynomials.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *  The following class is used in order to provide the graphical user interface. 
 *  It has the following components: a panel that will contain three labels 
 *  (“first polynomial”, “second polynomial ” and “result”), three texts fields 
 *  (two for introducing the polynomials and one for displaying the result) and 
 *  six buttons, one for each operation. This class also has a method to convert 
 *  the input String that is written in the text fields and a constructor used to 
 *  used to initialize and prepare the new object for use
 */

public class Gui extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JPanel panelFin;
	private JLabel l1, l2, result;
	private JTextField t1, t2, tResult;
	private JButton addButton, subButton, multiButton, divButton, intButton, difButton;
	
	private Polynomial transformToPolynomial(String s){
		
		Polynomial p = new Polynomial();
		String[] tempString = s.split("\\s+");
		int coefficient, degree;
		
		for(int i=0; i<tempString.length-1; i+=2) {
			coefficient = Integer.parseInt(tempString[i]);
			degree = Integer.parseInt(tempString[i+1]);
			p.addMonomialToPolinom(new IntMonomial(degree, coefficient));
		}
		
		return p;
	}
	
	public Gui(){
		
		super("Polynomial Operations");
		setLayout(new GridLayout());
		setSize(750,160);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
		panelFin = new JPanel();
		
		l1 = new JLabel("   First Polynomial:    ");
		l2 = new JLabel("Second polynomial: ");
		result = new JLabel("Operation Result:   ");
		
		t1 = new JTextField(55);
		t2 = new JTextField(55);
		tResult = new JTextField(45);
		tResult.setEditable(false);
		
		addButton = new JButton("Add");
		subButton = new JButton("Subtract");
		multiButton = new JButton("Multiply");
		divButton = new JButton("Divide");
		intButton = new JButton("Integrate first polynomial");
		difButton = new JButton("Differentiate first polynomial");
		
		/*
		 *  ActionListers have been added to each button in order to implement their functionality.  
		 */
		
		addButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				Polynomial p1, p2;
				
				try{	
					
					p1 = transformToPolynomial(t1.getText());
					p2 = transformToPolynomial(t2.getText());
					p1.addition(p2);
					p1.sort();
					
					if(p1.toString().equals("") == false)
						tResult.setText(""+p1);
					else
						tResult.setText("0");
					
				} catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error");
				}
				
			}
			
		});
		
		subButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				Polynomial p1, p2;
				
				try{	
					
					p1 = transformToPolynomial(t1.getText());
					p2 = transformToPolynomial(t2.getText());
					p1.subtraction(p2);
					p1.sort();
					
					if(p1.toString().equals("") == false)
						tResult.setText(""+p1);
					else
						tResult.setText("0");
					
				} catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error");
				}
				
			}
			
		});
		
		multiButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				Polynomial p1, p2;
				
				try{	
					
					p1 = transformToPolynomial(t1.getText());
					p2 = transformToPolynomial(t2.getText());
					p1.multiply(p2);
					p1.sort();
					
					if(p1.toString().equals("") == false)
						tResult.setText(""+p1);
					else
						tResult.setText("0");
					
				} catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error");
				}
				
			}
			
		});
		
		divButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				Polynomial p1, p2;
				String result;
				
				try{	
					
					p1 = transformToPolynomial(t1.getText());
					p2 = transformToPolynomial(t2.getText());
					result = p1.division(p2);
					tResult.setText(""+result);
					
				} catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error");
				}
				
			}
			
		});
		
		intButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				Polynomial p1;
				
				try{	
					
					p1 = transformToPolynomial(t1.getText());
					//p2 = transformToPolynomial(t2.getText());
					p1.integration();
					p1.sort();
					
					if(p1.toString().equals("") == false)
						tResult.setText(""+p1);
					else
						tResult.setText("0");
					
				} catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error");
				}
				
			}
			
		});
		
		difButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				Polynomial p1;
				
				try{	
					
					p1 = transformToPolynomial(t1.getText());
					//p2 = transformToPolynomial(t2.getText());
					p1.differentiation();
					p1.sort();
					
					if(p1.toString().equals("") == false)
						tResult.setText(""+p1);
					else
						tResult.setText("0");
					
				} catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error");
				}
				
			}
			
		});
		
	
		panelFin.add(l1);
		panelFin.add(t1);
		panelFin.add(l2);
		panelFin.add(t2);
		panelFin.add(addButton);
		panelFin.add(subButton);
		panelFin.add(multiButton);
		panelFin.add(divButton);
		panelFin.add(intButton);
		panelFin.add(difButton);
		panelFin.add(result);
		panelFin.add(tResult);
		add(panelFin);
		
		setVisible(true);
	}
	
}
